from django.contrib import admin


# Register your models here.
from ventas.models import Producto, Categoria, Factura, ProductoFactura

#@admin.register(Question,Choice)

class ProductoTabularInline(admin.TabularInline):
    model = Producto
    extra = 0

class ProductoStackedInline(admin.StackedInline):
    model = Producto
    extra = 0

class ProductoFacturaTabularInline(admin.TabularInline):
    model = ProductoFactura
    extra = 0

class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'categoria', 'presentacion', 'precio')
    readonly_fields = ['mi_nuevo_campo']

    def mi_nuevo_campo(self, objeto):
        #return objeto.nombre.str()+" - "+objeto.categoria.str()
        return "{} - {}".format(objeto.nombre, objeto.categoria)
    mi_nuevo_campo.short_description = "NUevo Campo"


admin.site.register(Producto, ProductoAdmin)


class CategoriaAdmin(admin.ModelAdmin):
    inlines = [ProductoTabularInline]


admin.site.register(Categoria, CategoriaAdmin)


class FacturaAdmin(admin.ModelAdmin):
    list_display = ('id','cliente', 'fecha')
    inlines = [ProductoFacturaTabularInline]
    readonly_fields = ['total']

    def total(self, object):
        #trae todos los producto factura donde factura = object
        #object es esta instancia
        items = ProductoFactura.objects.filter(factura=object)
        cantidad = 0
        for i in items:
            cantidad += i.cantidad * i.producto.precio
        return cantidad

admin.site.register(Factura, FacturaAdmin)


class ProductoFacturaAdmin(admin.ModelAdmin):
    list_display = ('producto', 'factura','cantidad')




admin.site.register(ProductoFactura, ProductoFacturaAdmin)