from django.db import models

# Create your models here.

# Create your models here.

class Categoria(models.Model):
    nombre = models.CharField(max_length=200,verbose_name="Categori")

    def __str__(self):
        return self.nombre

    """
    class Meta:
        verbose_name = u"Categori"
        verbose_name_plural = u"Categoriases"
        """




class Producto(models.Model):
    nombre = models.CharField(max_length=200)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    presentacion = models.CharField(max_length=200)
    precio = models.IntegerField()

    def __str__(self):
        return self.nombre

class Factura(models.Model):
    fecha = models.DateTimeField()
    cliente = models.CharField(max_length=200)

    def __str__(self):
        return "{}".format(self.id)


class ProductoFactura(models.Model):
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    factura = models.ForeignKey(Factura,  on_delete=models.CASCADE)
    cantidad = models.IntegerField()

    class Meta:
        verbose_name = u"Item"
        verbose_name_plural = u"Items de Factura"

