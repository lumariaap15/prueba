from django.contrib import admin


# Register your models here.
from polls.models import Question, Choice


def marcar_publicada(modeladmin, request, queryset):
    queryset.update(status='p')




marcar_publicada.short_description = "Marcar como publicadas"


#@admin.register(Question,Choice)


class QuestionAdmin(admin.ModelAdmin):
    list_display = ['question_text', 'fecha','status']
    #list_display_links = None
    ordering = ['question_text']
    actions = [marcar_publicada]
    date_hierarchy = 'fecha'
    search_fields = ['question_text']
    readonly_fields = ['fecha']


admin.site.register(Question, QuestionAdmin)


class ChoiceAdmin(admin.ModelAdmin):
    #autocomplete_fields = ['question']
    raw_id_fields = ['question']
    list_filter = ['question', 'choice_text', 'votes']


admin.site.register(Choice, ChoiceAdmin)




