from django.db import models

# Create your models here.
ESTADO_PREGUNTA = (
    ('p','Publicada'),
    ('r','En proceso de Revisión'),
    ('c','Cerrada')
)

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    fecha = models.DateTimeField('fecha publicacion')
    status = models.CharField(max_length=1, choices=ESTADO_PREGUNTA)

    def __str__(self):
        return self.question_text

class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text